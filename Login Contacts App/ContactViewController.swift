//
//  ContactViewController.swift
//  Login Contacts App
//
//  Created by Francisco Morales on 10/27/17.
//  Copyright © 2017 Francisco Morales. All rights reserved.
//

import UIKit
import Parse

class ContactViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    //STORING RETREIVED DATA
    var retreivedContacts = [PFObject]()
    
    //OUTLET
    @IBOutlet var contactsTableView: UITableView!
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return retreivedContacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let contactRow = retreivedContacts[indexPath.row]
        cell.textLabel?.text = contactRow["name"] as! String
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.performSegue(withIdentifier: "addNew", sender: retreivedContacts[indexPath.row])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addNew" {
            let destination = segue.destination as! ContactInfoViewController
            destination.incomingContact = sender as? PFObject
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        getData()
        contactsTableView.dataSource = self
        contactsTableView.delegate = self
        reload()

        }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getData() {
        var query = PFQuery(className:"Contact")
            query.order(byAscending: "name")
            query.findObjectsInBackground { (objects, error) in
            if error == nil {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) contacts.")
                // Do something with the found objects
                if let objects = objects {
                    for object in objects {
                        print(object.objectId!)
                    }
                }
                self.retreivedContacts = objects as! [PFObject]
                self.contactsTableView.reloadData()
            } else {
                // Log details of the failure
                print("Error: \(error!) \(error!._userInfo ?? "" as AnyObject)")
            }

            }
    }

    func reload() {
    contactsTableView.reloadData()
    }
    
    @IBAction func logoutButton(_ sender: Any) {
        PFUser.logOutInBackground { (error) in
            print(PFUser.current()?["email"] as? String )
        }
    }
}
