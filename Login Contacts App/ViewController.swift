//
//  ViewController.swift
//  Login Contacts App
//
//  Created by Francisco Morales on 10/25/17.
//  Copyright © 2017 Francisco Morales. All rights reserved.
//

import UIKit
import Parse

class ViewController: UIViewController {
    
    //OUTLETS
    @IBOutlet var loginEmail: UITextField!
    @IBOutlet var loginPassword: UITextField!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func loginButton(_ sender: Any) {
        Login()
    }
    
    func Login() {
        [PFUser.logInWithUsername(inBackground: loginEmail.text!, password: loginPassword.text!, block: { (user, error) in
            if(user != nil){
                print("VALID LOGIN")
                //
                //DispatchQueue.main.async {
                    let storyboard = UIStoryboard(name: "Main", bundle:nil)
                    let viewController = storyboard.instantiateViewController(withIdentifier: "Main") 
                    self.present(viewController, animated: true)
                    //self.test()
               // }
                //
            }
            else {
                print("BAD LOGIN")
                //Bad Login Alert
                let alertController = UIAlertController(title: "Woah there turbo!", message:
                    "Invalid Account. Try again!", preferredStyle: UIAlertControllerStyle.alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                self.present(alertController, animated: true, completion: nil)

            }
        })]
        
        if (PFUser.current() != nil) {
            print(PFUser.current()?["email"] as? String )
        }
        
    }
    
    func test(){
        var saveContact = PFObject(className: "Contact")
        saveContact["name"] = "nameTextField.text"
        saveContact["phone"] = "phoneTextField.text"
        saveContact["emailcontact"] = "emailTextField.text"
        saveContact["address"] = "addressTextField.text"
        saveContact.saveInBackground {
            (success: Bool, error: Error?) in
            if (success) {
                print("success")
            } else {
                print("error")
            }
        }
    }//end test
    
}

