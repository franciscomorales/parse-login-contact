//
//  ContactInfoViewController.swift
//  Login Contacts App
//
//  Created by Francisco Morales on 10/28/17.
//  Copyright © 2017 Francisco Morales. All rights reserved.
//

import UIKit
import Parse

class ContactInfoViewController: UIViewController, UITextFieldDelegate {

    //OUTLETS
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var phoneTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var addressTextField: UITextField!
    
    var incomingContact: PFObject? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //DELEGATE TO DISMISS KEYBOARD
        self.nameTextField.delegate = self
        self.phoneTextField.delegate = self
        self.emailTextField.delegate = self
        self.addressTextField.delegate = self
        
        if (incomingContact != nil) {
            nameTextField.text = incomingContact?["name"] as? String
            phoneTextField.text = incomingContact?["phone"] as? String
            emailTextField.text = incomingContact?["email"] as? String
            addressTextField.text = incomingContact?["address"] as? String
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveButton(_ sender: Any) {
        save()
        //Save Alert
        let alertController = UIAlertController(title: "Saved Contact!", message:
            "", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
        self.present(alertController, animated: true, completion: nil)
        
        //Go Back to Contacts Table View
        performSegue(withIdentifier: "goBack", sender: Any?.self)
    }
    
    func save() {
        var saveContact = PFObject(className: "Contact")
        saveContact["name"] = nameTextField.text
        saveContact["phone"] = phoneTextField.text
        saveContact["emailcontact"] = emailTextField.text
        saveContact["address"] = addressTextField.text
        saveContact.saveInBackground {
            (success: Bool, error: Error?) in
            if (success) {
                print("success")
            } else {
                print("error")
            }
        }

    }
    
}
