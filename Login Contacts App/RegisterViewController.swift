//
//  RegisterViewController.swift
//  Login Contacts App
//
//  Created by Francisco Morales on 10/27/17.
//  Copyright © 2017 Francisco Morales. All rights reserved.
//

import UIKit
import Parse

class RegisterViewController: UIViewController, UITextFieldDelegate {

    //OUTLETS
    @IBOutlet var registerEmail: UITextField!
    @IBOutlet var registerPassword: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func registerButton(_ sender: Any) {
        register()
    }
    
    func register() {
        var user = PFUser()
        user.username = registerEmail.text
        user.password = registerPassword.text
        user.email = registerEmail.text
        
        user.signUpInBackground { (success, error) in
            if let error = error {
                //                let errorString = e._userInfo!["error"] as? NSString
                // Show the errorString somewhere and let the user try again.
                let alertController = UIAlertController(title: "Registeration Failure. Try Again", message: "", preferredStyle: UIAlertControllerStyle.alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default, handler: nil))
                self.present(alertController, animated: true, completion: nil)
                
            } else {
                print("Registration Successful")
                
                let alertController = UIAlertController(title: "Registeration Success", message: "", preferredStyle: UIAlertControllerStyle.alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default, handler: nil))
                self.present(alertController, animated: true, completion: nil)
                
                self.registerEmail.text = ""
                self.registerPassword.text = ""
                
                self.performSegue(withIdentifier: "fromRegisterToLogin", sender: Any?.self)
            }
        }

    }

}
